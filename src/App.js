import React, {Component} from 'react';
import logo from './todo.png';
import './App.css';
import {Task} from "./TaskRow/Task";

class App extends Component {

    state = {
        task: [
            {id: 1, name:"First Task" },
        ],
        showTask: true
    };
    handleAddButtonAction = (event) =>{
        this.setState({
            task: [{id:Math.random(), name:event.target.value}]
        })
    };
    onChange = (event) =>{
        this.state({task: event.target.value})
    };

    onSubmit = (event)=> {
        event.preventDefault()
        this.setState({
            task:[...this.state.task,this.state.task]
        });
    };


  render() {
      let task = null;

      if (this.state.showTask) {
          task = (
              <div>
                  {this.state.task.map((task, index) =>{
                      return <Task
                      name={task.name}
                      />
                  })}
              </div>
          );
      }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Simple TO DO</h1>
        </header>
          <form>
        <div className="App-intro">
            <input
                type="text"
            />
        </div>
              <input value={this.state.task} onChange={this.onChange}/>
          <button >Add Task</button>
          </form>
          <Task/>
          <form className="App" onSubmit={this.onSubmit}>
              <input value={this.state.task} onChange={this.onChange}/>
              <button>Submit</button>
          </form>
      </div>
    );
  }
}

export default App;
